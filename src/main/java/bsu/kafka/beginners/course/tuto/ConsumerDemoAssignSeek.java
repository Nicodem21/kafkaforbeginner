package bsu.kafka.beginners.course.tuto;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsumerDemoAssignSeek {

	public static void main(String[] args) {
		
		Logger logger = LoggerFactory.getLogger(ConsumerDemoAssignSeek.class);
		
		String bootstrapServer = "127.0.0.1:9092";
		String topic = "my-second-topic";
		
		// create the consumer configs
		Properties properties = new Properties();
		properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
		// le consumer reçoi le message sérialisé en octect par le producer et la désérialise pour obtenir le message
		properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		
		// create the consumer
		KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(properties);
		
		// assign and seek are mostly used to replay data or fetch a specific message
		
		//assign
		TopicPartition patitionToReadFrom = new TopicPartition(topic, 0);
		long offsetToReadFrom = 0L;
		consumer.assign(Arrays.asList(patitionToReadFrom));
		
		//seek
		consumer.seek(patitionToReadFrom, offsetToReadFrom);
		
		int numberOfMessagesToRead = 5;
		boolean keepOnReading = true;
		int numberOfMessagesReadSoFar = 0;

		//poll for new data
		while (keepOnReading) {
			ConsumerRecords<String, String> records = 
					consumer.poll(Duration.ofMillis(100)); // Duration.ofMillis(100) est nouveau sur la version2.0.0 de kafka. avant: .poll(100)
			
			for(ConsumerRecord<String, String> record: records) {
				numberOfMessagesReadSoFar +=1;
				logger.info("Key: "+record.key()+", Value: "+record.value());
				logger.info("Partition: "+record.partition()+", Offset: "+record.offset());
				if (numberOfMessagesReadSoFar>=numberOfMessagesToRead) {
					keepOnReading = false; //to exit the while loop
					break; //to exit the for loop 
				}
			}
		}
		logger.info("Exiting the application");
	}

}
