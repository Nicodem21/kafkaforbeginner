package bsu.kafka.beginners.course.tuto;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsumerDemoWithThread {

	public static void main(String[] args) {

		new ConsumerDemoWithThread().run();
	}

	private ConsumerDemoWithThread() {

	}

	private void run() {
		Logger logger = LoggerFactory.getLogger(ConsumerDemoWithThread.class);

		String bootstrapServer = "127.0.0.1:9092";
		String groupId = "my-sixth-application";
		String topic = "my-second-topic";

		// latch for dealing with multiple threads
		CountDownLatch latch = new CountDownLatch(1);

		// create the consumer runnable
		logger.info("Creating the consumer thread");
		Runnable myConsumerRunnable = new ConsumerRunnable(latch, bootstrapServer, groupId, topic);

		// start the thread
		Thread myThread = new Thread(myConsumerRunnable);
		myThread.start();

		// add shotdown hook
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			logger.info("Caught shutdown hook");
			((ConsumerRunnable) myConsumerRunnable).shutdown();
			
			try {
				latch.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			logger.info("Application has exited !");
		}));

		try {
			latch.await();
		} catch (InterruptedException e) {
			logger.error("Application got interrupted", e);
		} finally {
			logger.info("Application is closing");
		}
	}

	public class ConsumerRunnable implements Runnable {

		private CountDownLatch latch;

		KafkaConsumer<String, String> consumer;

		private Logger logger = LoggerFactory.getLogger(ConsumerRunnable.class);

		public ConsumerRunnable(CountDownLatch latch, String bootstrapServer, String groupId, String topic) {
			this.latch = latch;

			// create the consumer configs
			Properties properties = new Properties();
			properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
			// le consumer reçoi le message sérialisé en octect par le producer et la
			// désérialise pour obtenir le message
			properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
			properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
			properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
			properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

			// create the consumer
			consumer = new KafkaConsumer<String, String>(properties);

			// subscribe consumer to our topic
			consumer.subscribe(Arrays.asList(topic));
		}

		public void run() {
			// poll for new data
			try {
				while (true) {
					ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100)); // Duration.ofMillis(100) est nouveau sur la version2.0.0 de kafka avant: .poll(100)

					for (ConsumerRecord<String, String> record : records) {
						logger.info("Key: " + record.key() + ", Value: " + record.value());
						logger.info("Partition: " + record.partition() + ", Offset: " + record.offset());
					}
				}
			} catch (WakeupException e) {
				logger.info("Received shutdown signal !");
			} finally {
				consumer.close();
				// tell our code we're done with the consumer
				latch.countDown();
			}

		}

		public void shutdown() {
			// the wakeup() method is a special method to interrupt consummer.poll()
			// it will throw the exception WakeUpException
			consumer.wakeup();
		}

	}

}
