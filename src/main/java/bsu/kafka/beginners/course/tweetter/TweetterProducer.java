package bsu.kafka.beginners.course.tweetter;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.Hosts;
import com.twitter.hbc.core.HttpHosts;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

public class TweetterProducer{
	
	Logger logger = LoggerFactory.getLogger(TweetterProducer.class);
	
	String consumerkey = "5pxrcQE9iHVdp90XTn00DsO5N";
	String consumerSecret = "W7jmfmnSgixHYtTvrJhdPMV0hrdSwebMAQMUGEnYpKGut9rpdr";
	String token = "874970872402104320-6tPa59o6tjiy377g6hIHm7iP7xS3wGW";
	String secret = "1ocvMQAKmTwf9FG7kpTFMK4H41JrEvFb4A84mLZTxhRZI";
	
	List<String> terms = Lists.newArrayList("kafka");
	
	public TweetterProducer() {}

	public static void main(String[] args) {
		
		// call the method run() to run our application
		new TweetterProducer().run();
	
	}
	
	// The run method
	public void run() {
		
		logger.info("Setup");
		/** Set up your blocking queues: Be sure to size these properly based on expected TPS of your stream */
		BlockingQueue<String> msgQueue = new LinkedBlockingQueue<String>(1000);
		
		Client client = createTwitterClient(msgQueue);
		
		// create a tweetter client
		client.connect(); // Attempts to establish a connection.
		
		//create a kafka producer
		KafkaProducer<String, String> producer = createKafkaProducer();
		
		// add a shutdown hook
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			logger.info("Stopping apllication...");
			logger.info("Shutting down client from tweetter...");
			client.stop();
			logger.info("Closing producer");
			producer.close();
			logger.info("Done!");
		}));
		
		//loop to send tweets to kafka
		// on a different thread, or multiple different threads....
		while (!client.isDone()) {
		   String msg = null;
		   try {
			   msg = msgQueue.poll(5, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
			client.stop();
		}
		
		   if(msg != null) {
			   logger.info(msg); 
			   producer.send(new ProducerRecord<String, String>("tweetter_tweets", null, msg), new Callback() {
				
				@Override
				public void onCompletion(RecordMetadata metadata, Exception exception) {
					if(exception != null) {
						logger.error("Something bad happened", exception);
					}
				}
			});
		   }
		   
		  logger.info("End of application");
		}
		
	}
	
	
	public Client createTwitterClient(BlockingQueue<String> msgQueue) {
		
		/** Declare the host you want to connect to, the endpoint, and authentication (basic auth or oauth) */
		Hosts hosebirdHosts = new HttpHosts(Constants.STREAM_HOST);
		StatusesFilterEndpoint hosebirdEndpoint = new StatusesFilterEndpoint();
		
		// Optional: set up some followings and track terms
		List<Long> followings = Lists.newArrayList(1234L, 566788L);
		hosebirdEndpoint.trackTerms(terms);

		// These secrets should be read from a config file
		Authentication hosebirdAuth = new OAuth1(consumerkey, consumerSecret, token, secret);
		
		
		ClientBuilder builder = new ClientBuilder()
				  .name("Hosebird-Client-01")                              // optional: mainly for the logs
				  .hosts(hosebirdHosts)
				  .authentication(hosebirdAuth)
				  .endpoint(hosebirdEndpoint)
				  .processor(new StringDelimitedProcessor(msgQueue));
				Client hosebirdClient = builder.build();
				
				return hosebirdClient;
	}
	
	public KafkaProducer<String, String> createKafkaProducer(){
		String bootstrapServer = "127.0.0.1:9092";
		// create producer properties
		Properties properties = new Properties();
		properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
		properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		
		//create the producer
		KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);
		return producer;
	}
}
